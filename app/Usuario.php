<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
// use App\Presenters\LoginPresenter;


class Usuario extends Model
{
  protected $table = 'usuarios';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];

//   public function present()
//   {
//       return new LoginPresenter($this);
//   }

// 
}
