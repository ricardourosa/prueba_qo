<?php

namespace App\Http\Controllers;
use App\Usuario;
use App\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class Usuarios extends Controller
{
    public function index()
    {
       
        
    }

    public function store(Request $request)
    {
        // $validarUsuario = Usuario::where('contraseña',Hash::make($request->input('password')))
        //                 //   ->where('login',Hash::make($request->input('rut')))
                        //   ->get();
        
        $validarUsuario = Usuario::where('contraseña',$request->input('password'))
                          ->where('login',$request->input('rut'))
                          ->first();
                          
            

         $cliente = Cliente::findOrFail($validarUsuario->clientes_id);
         
         $nombreCliente = $cliente->nombre.' '.$cliente->apellido;   
        //  dd($nombreCliente);
         if($validarUsuario){
            return view('menu', compact('nombreCliente'));
        }else{
            return view('welcome');
        }
    }

}
