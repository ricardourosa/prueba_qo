<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="7bvi1EFwE9orX0T8AC5atkoG48jawB_Neyi2orVhl0s" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


      {{-- plantilla: --}}
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
      <link rel="stylesheet" href="{{ asset('/css/me.css') }}">
      <style type="text/css">
       span.red {
            background: red;
            border-radius: 0.8em;
            -moz-border-radius: 0.8em;
            -webkit-border-radius: 0.8em;
            color: #ffffff;
            display: inline-block;
            font-weight: bold;
            line-height: 1em;
            margin-right: 15px;
            text-align: center;
            width: 1em;
            }
            span.green {
            background: #5EA226;
            border-radius: 0.8em;
            -moz-border-radius: 0.8em;
            -webkit-border-radius: 0.8em;
            color: #ffffff;
            display: inline-block;
            font-weight: bold;
            line-height: 1em;
            margin-right: 15px;
            text-align: center;
            width: 1em;
        }

        </style>

</head>
<body>
  
    <!-- Sidebar menu-->
    @include('layouts.menu')
   
        @yield('body')
    {{-- @endif --}}
    {{-- footer --}}
    @include('layouts.footer')
  

</body>
</html>
