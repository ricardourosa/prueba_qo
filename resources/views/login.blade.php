@extends('layouts.app')

@section('content')

    <div class="logoLogin"><img src="{{ asset('/img/logo.png') }}"></div>
    <form class="loginForm" id="filterForm" method="POST" action="{{ route('login') }}">
        @csrf
      <div class="title">Ingreso al sistema</div>
      <div class="form-group">
        <label>{{ __('E-Mail Address') }}</label>
        <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

      </div>
      <div class="form-group">
        <label>{{ __('Password') }}</label>
        <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert" style="color: white;">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif

      </div>

        @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert" style="color: white;">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif

      <div class="message">
        @if (Route::has('password.request'))
            <a class="btn btn-link" style="color: white;" href="{{ route('password.request') }}">
                <p>{{ __('¿Olvido su contraseña?') }}</p>
            </a>
        @endif
      </div>
      <button type="submit" class="sbtn submit"> {{ __('Ingresar') }} </button>
    </form>

@endsection
