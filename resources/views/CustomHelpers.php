<?php

/* ********************  FUNCIONES INTERNAS: *************************** */

function armar_text_select($element,$texts,$separador){
  $cant = count($texts);
  $text = '';
  for ($i=0; $i < $cant ; $i++) {
    $propiedad = $texts[$i];
    if($i==0) $text .= $element->$propiedad;
    else $text .= ' '.$separador.' '.$element->$propiedad;
  }
  return $text;
}

/* ---------------------- COLUMNAS DE LAS TABLAS: ---------------------------- */

//armar links de orden ASC y DESC de las columnas de las tablas
function order_column($column_name, $field_data, $sorted){
  $down_up  = 'keyboard_arrow_down';
  if(!is_null(request('orderby'))){
    if( request('orderby') == $field_data){

      if(!is_null(request('sorted'))){
        $sorted   = (request('sorted')=='DESC') ? 'ASC' : 'DESC';
        $down_up  = (request('sorted')=='ASC') ? 'keyboard_arrow_down' : 'keyboard_arrow_up';
      }else{
        $sorted   = 'ASC';
        $down_up  = 'keyboard_arrow_down';
      }

    }
  }

  return $column_name . "<a href=". Request::fullUrlWithQuery(['orderby' => $field_data,'sorted'=> $sorted]) ."><div class='material-icons'>". $down_up ."</div></a>";
}

/* ---------------------- COMPONENTES DE LOS FORMS FILTROS: ---------------------------- */

//armar cualquier filtro Multiple de una vista con un objeto:
function optionsSelectObjetfilterMultiple($datos_filter,$value,$texts,$separador=''){

  $options_select = '';

  foreach($datos_filter as $element){
    if( !empty(request()->query($value)) ){
      $selected = '';

      if(in_array($element->$value, request()->query($value))){ $selected = 'selected="selected"'; }
      $options_select .= '<option value="'. $element->$value .'" '.$selected.'> '. armar_text_select($element,$texts,$separador) .'</option>';
    }else{
        //dd($element->$value);
      $options_select .= '<option value="'. $element->$value .'" selected="selected"> '. armar_text_select($element,$texts,$separador) .'</option>';
    }
  }//fin foreaach
// dd($options_select);
  return $options_select;
}

//armar cualquier filtro Multiple de una vista con un Array asociativo:
function optionsSelectArrayfilterMultiple($datos_filter,$value){
  $options_select = '';
  foreach($datos_filter as $key=>$valor){
    if( !empty(request()->query($value)) ){
      $selected = '';
      if(in_array($key, request()->query($value))){ $selected = 'selected="selected"'; }
      $options_select .= '<option value="'. $key .'" '.$selected.'> '. $valor .'</option>';
    }else{
      $options_select .= '<option value="'. $key .'" selected="selected"> '. $valor .'</option>';
    }
  }//fin foreaach

  return $options_select;
}

//armar cualquier filtro Simple de una vista con un objeto:
function optionsSelectObjetfilterSimple($datos_filter,$value,$texts,$separador='',$all_opc=false){
  $options_select = '';
  if($all_opc)
    $options_select = '<option value="all" selected>Todos</option>';

  foreach($datos_filter as $element){
    if( !empty(request()->query($value)) ){
      $selected = '';
      if($element->$value == request()->query($value)){ $selected = 'selected="selected"'; }
      $options_select .= '<option value="'. $element->$value .'" '.$selected.'> '. armar_text_select($element,$texts,$separador) .'</option>';
    }else{
      $options_select .= '<option value="'. $element->$value .'"> '. armar_text_select($element,$texts,$separador) .'</option>';
    }
  }//fin foreaach

  return $options_select;
}

//armar cualquier filtro Simple de una vista con un Array asociativo:
function optionsSelectArrayfilterSimple($datos_filter,$value, $all_opc=false){
  $options_select = '';
  if($all_opc)
    $options_select = '<option value="all" selected>Todos</option>';
  foreach($datos_filter as $key=>$valor){
    if( !empty(request()->query($value)) ){
      $selected = '';
      if($key == request()->query($value)){ $selected = 'selected="selected"'; }
      $options_select .= '<option value="'. $key .'" '.$selected.'> '. $valor .'</option>';
    }else{
      $options_select .= '<option value="'. $key .'"> '. $valor .'</option>';
    }
  }//fin foreaach

  return $options_select;
}


/* ---------------------- COMPONENTES DE LOS FORMS CREATE/EDIT: ---------------------------- */

function armarInputCreateEdit($formato, $key, $title , $type, $errors, $objeto ,$class_input, $required, $placeholder)
{
    $class_error = $errors->has($key) ? ' error' : '';
    $span_error = ($errors->has($key)) ? '<span class="bottom-description text-danger"><strong>* '. $errors->first($key) .'</strong></span>' : '';
    $value = old($key, isset($objeto->$key) ? $objeto->$key : null);

    if($type == 'readonly')
      return '<div class="'.$formato.'">
                <div class="form-group completed">
                  <label>'. $title .'</label>
                  <span>'.$value.'</span>
                </div>
              </div>';
    else
      return '<div class="'.$formato.'">
              <div class="form-group'. $class_error .'">
                <label>'. $title .'</label>
                <input type="'.$type.'" id="'.$key.'" name="'.$key.'" value="'.$value.'" class="'.$class_input.'" '.$required.' placeholder="'.$placeholder.'">
                '.$span_error.'
              </div>
            </div>';
}

function armarInputCreateEdit1($formato, $key, $title , $type, $errors, $objeto ,$class_input, $required, $placeholder,$maxlength)
{
    $class_error = $errors->has($key) ? ' error' : '';
    $span_error = ($errors->has($key)) ? '<span class="bottom-description text-danger"><strong>* '. $errors->first($key) .'</strong></span>' : '';
    $value = old($key, isset($objeto->$key) ? $objeto->$key : null);

    if($type == 'readonly')
      return '<div class="'.$formato.'">
                <div class="form-group completed">
                  <label>'. $title .'</label>
                  <span>'.$value.'</span>
                </div>
              </div>';
    else
      return '<div class="'.$formato.'">
              <div class="form-group'. $class_error .'">
                <label>'. $title .'</label>
                <input type="'.$type.'" id="'.$key.'" name="'.$key.'" value="'.$value.'" class="'.$class_input.'" '.$required.' maxlength="'.$maxlength.'" placeholder="'.$placeholder.'" onKeyPress="return soloNumeros(event)">
                '.$span_error.'
              </div>
            </div>';
}

//armar cualquier filtro de un formulario con un objeto:
function armarSelectObjectCreateEdit($options, $val, $texts, $separador, $formato, $key, $title , $errors, $objeto ,$class_select)
{
  $class_error = $errors->has($key) ? ' error' : '';
  $span_error = ($errors->has($key)) ? '<span class="bottom-description text-danger"><strong>* '. $errors->first($key) .'</strong></span>' : '';
  $value = isset($objeto->$key) ? $objeto->$key : null;

  $optionsAux = '';
  foreach($options as $element){
    $selected = ''; if($element->$val == $value ){ $selected = 'selected="selected"'; }
    $optionsAux .= '<option value="'. $element->$val .'" '.$selected.'> '. armar_text_select($element,$texts,$separador) .'</option>';
  }//fin foreaach

  return '<div class="'.$formato.'">
            <div class="form-group'. $class_error .'">
              <label>'. $title .'</label>
              <select id="'.$key.'" name="'.$key.'" class="'.$class_select.'">
                 '.$optionsAux.'
              </select>
              '.$span_error.'
            </div>
          </div>';
}

//armar cualquier filtro de un formulario con un Array asociativo:
function armarSelectArrayCreateEdit($options, $formato, $key, $title , $errors, $objeto ,$class_select)
{
  if($options == 'readonly')
    return '<div class="'.$formato.'">
              <div class="form-group completed">
                <label>'. $title .'</label>
                <span>'.$objeto->$key.'</span>
              </div>
            </div>';


  $class_error = $errors->has($key) ? ' error' : '';
  $span_error = ($errors->has($key)) ? '<span class="bottom-description text-danger"><strong>* '. $errors->first($key) .'</strong></span>' : '';
  $value = isset($objeto->$key) ? $objeto->$key : null;

  $optionsAux = '';
  foreach($options as $clave=>$val){
    $selected = ''; if($clave == $value ){ $selected = 'selected="selected"'; }
    $optionsAux .= '<option value="'. $clave .'" '.$selected.'> '. $val .'</option>';
  }//fin foreaach

  return '<div class="'.$formato.'">
            <div class="form-group'. $class_error .'">
              <label>'. $title .'</label>
              <select id="'.$key.'" name="'.$key.'" class="'.$class_select.'">
                 '.$optionsAux.'
              </select>
              '.$span_error.'
            </div>
          </div>';
}

?>
