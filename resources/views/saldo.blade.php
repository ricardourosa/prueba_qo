@extends('layouts.app')


@section('body')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 col-md-3">
                
          <div id="filterForm">
            <div class="toggleButton" toggle="filter">
              <div class="material-icons">keyboard_arrow_left</div>
            </div>
            
             <form action="{{route('deposito')}}" method="POST">
              @csrf
            <button class="sbtn submit">Depositos</button>
            </form>
            <form action="{{route('transferencia')}}" method="POST">
            @csrf
            <button class="sbtn submit">Transferencias</button>
            
            </form>
            <form action="{{route('saldo')}}" method="POST">
            @csrf
            <button class="sbtn submit">Saldo Diponible</button>
            </form>
            <form action="{{route('historico')}}" method="POST">
            @csrf
            <button class="sbtn submit">Historicos</button>
            </form>
          </div>
        
        </div>
        <div class="col-xs-12 col-md-9">
          <div class="row">
            <div class="col-xs-12 col-sm-9">
              <div class="sectionTitle">Desposito
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 pull-right">
            </div>
          </div>
          <!-- flash respuesta: -->
          @if( session()->has('success') || session()->has('error') )
            <div class="alert {{ session()->has('success')? 'alert-success' : 'alert-danger' }}" role="alert">
              <span class="alert-inner--icon"><i class="fe fe-thumbs-up"></i></span>
              @if(session()->has('success'))
                <span class="alert-inner--text"><strong>Exito</strong> {{ session('success') }}</span>
              @else
                <span class="alert-inner--text"><strong>Error</strong> {{ session('error') }}</span>
              @endif
            </div>
          @endif

					<div class="normalForm">
						<!-- formulario: -->
						<form method="POST" action="{{ route('depositoUpdate', 'a') }}" >
							{{-- @method('PUT') --}}
	            @csrf
	           <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group completed">
                  <label>Monto a Depositar</label>
                  <input type="text" id="cant" name="cant" value=""    onKeyPress="return soloNumeros(event)">
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group completed">
                  <label>Cuenta a Depositar</label>
                  <input type="text" id="cuenta" name="cuenta" value=""    onKeyPress="return soloNumeros(event)">
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group completed">
                  <label>Despositante</label>
                  <input type="text" id="depositante" name="depositante" value=""  >
                </div>
              </div>
               <div class="row">
                  <div class="col-xs-12 col-sm-4 col-md-3 pull-right">
                    <button type="submit" class="sbtn submit wait" >Despositar</button>
                  </div>
                  
                </div>
						</form>
						<!-- fin formulario: -->
					</div>

        </div>
      </div>
    </div>
  </div>

  {{-- scripts: --}}
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript" src="{{ asset('/js/base.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/project.js') }}"></script>

  <script type="text/javascript" >
    function soloNumeros(e)
    {
        var key = window.Event ? e.which : e.keyCode
        return ((key >= 48 && key <= 57) || (key==8))
    }


  </script>
  
    


@endsection
