@extends('layouts.app')


@section('body')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 col-md-3">
                
          <div id="filterForm">
            <div class="toggleButton" toggle="filter">
              <div class="material-icons">keyboard_arrow_left</div>
            </div>
            
            <form action="{{route('deposito')}}" method="POST">
              @csrf
            <button class="sbtn submit">Depositos</button>
            </form>
            <form action="{{route('transferencia')}}" method="POST">
            @csrf
            <button class="sbtn submit">Transferencias</button>
            
            </form>
            <form action="{{route('saldo')}}" method="POST">
            @csrf
            <button class="sbtn submit">Saldo Diponible</button>
            </form>
            <form action="{{route('historico')}}" method="POST">
            @csrf
            <button class="sbtn submit">Historicos</button>
            </form>
          </div>
        
        </div>
        <div class="col-xs-12 col-md-9">
          <div class="row">
            <div class="col-xs-12 col-sm-9">
              <div class="sectionTitle">Bienvenidos
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 pull-right">
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="table-responsive">
                <table class="table actions table-hover" id="table-empresas">
                  <thead>
                    
                  </thead>
                  <tbody>
                   
                    



                  </tbody>
                </table>
              </div>
         
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- scripts: --}}
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript" src="{{ asset('/js/base.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/js/project.js') }}"></script>

  
  
    


@endsection
