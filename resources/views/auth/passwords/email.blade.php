@extends('layouts.app')

@section('content')
  <div class="logoLogin"><img src="{{ asset('/img/logo.png') }}"></div>

  @if (session('status'))
    <div class="container">
      <div class="alert alert-success" role="alert"> 
        <span class="alert-inner--text">{{ session('status') }}</span> 
      </div>
    </div>
  @endif

  <form class="loginForm" id="filterForm" method="POST" action="{{ route('password.email') }}">
    @csrf
    <div class="title">Reset Contraseña</div>

    <div class="form-group">
        <label>{{ __('E-Mail Address') }}</label>
        <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert" style="color: white;">
              {{ $errors->first('email') }}
            </span>
        @endif
    </div>

    <button type="submit" class="sbtn submit"> {{ __('Enviar Email') }} </button>

  </form>
@endsection
