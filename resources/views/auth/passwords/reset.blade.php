@extends('layouts.app')

@section('content')

  <style> .loginForm .form-group {margin-bottom: 22px;} </style>
  <div class="logoLogin"><img src="{{ asset('/img/logo.png') }}"></div>
        
  <form  class="loginForm" id="filterForm" method="POST" action="{{ route('password.update') }}">
      @csrf

      <input type="hidden" name="token" value="{{ $token }}">

      <div class="title">actualizar contraseña</div>

       <div class="form-group">
        <label>{{ __('E-Mail Address') }}</label>
        <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>
        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert" style="color: white;">
              {{ $errors->first('email') }}
            </span>
        @endif
      </div>

      <div class="form-group">
        <label>{{ __('Contraseña') }}</label>
        <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert" style="color: white;">
              {{ $errors->first('password') }}
            </span>
        @endif
      </div>

      <div class="form-group">
        <label>{{ __('Confirmar Contraseña') }}</label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
      </div>

      
      <button type="submit" class="sbtn submit"> {{ __('Actualizar Contraseña') }} </button>
  </form>
                
@endsection
