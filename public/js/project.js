$('.toggleButton[toggle=menu]').on('click',function(){
	$('.nav').toggleClass('show');
	$('body').toggleClass('menuDisplay');
	$('#filterForm').removeClass('show');
})

$('.toggleButton[toggle=filter]').on('click',function(){
	$('#filterForm').toggleClass('show');
	$('body').toggleClass('menuDisplay');
	$('.nav').removeClass('show');
})

$('.nav>ul>li').on('click',function(){
	$(this).find('ul').toggleClass('show');
})

$(window).resize(function(){
	$('.nav').removeClass('show');
	$('.ul').removeClass('show');
	$('#filterForm').removeClass('show');
	$('body').removeClass('menuDisplay');
})

// Inicializar Multiselect
//==========================================================>

	// Para masculino agregar la clase .mas
	//==========================================================>
		$(document).ready(function(){
			$('select.normal').multiselect({
				enableClickableOptGroups: true, //En caso de existir grupos, permite seleccionar el grupo completo
				enableCollapsibleOptGroups: true, //En caso de existir grupos, permite colapsarlos
				disableIfEmpty: true, //Inhabilita en caso de no tener opciones
				disabledText: 'Seleccione para busqueda..', //Texto en caso de que no existan opciones
				maxHeight: 200, //Define el máximo de alto a desplegar
				buttonClass: 'selectBtn', //Selecciona la clase que se aplicará al botón
				numberDisplayed: 1, //Límite que se establece para ocupar el texto nSelectedText
				delimiterText: ';', //Separador de alternativas en el botón cuando numberDisplayed > 1
				selectedClass: 'selectedOption', //Clase de opciones seleccionadas
				includeSelectAllOption: true, //Agrega opción de seleccionar todas
				allSelectedText: 'Todos los valores', //Texto que se muestra cuando todas las opciones están seleccionadas
				nSelectedText: 'Valores seleccionados', //Texto cuando el límite de selección (numberDisplayed) es alcanzado
				selectAllText: 'Seleccionar todos los valores', //Texto de includeSelectAllOption
				onDropdownShow: function(event) {
					$(this.$select).parents('.form-group').addClass('focus')
				},
				onDropdownHide: function(event) {
					$(this.$select).parents('.form-group').removeClass('focus')
				},
			});
		});

	// Con buscador agregar la clase .search
	//==========================================================>
		$(document).ready(function(){
			$('select.search').multiselect({
				enableClickableOptGroups: true, //En caso de existir grupos, permite seleccionar el grupo completo
				enableCollapsibleOptGroups: true, //En caso de existir grupos, permite colapsarlos
				disableIfEmpty: true, //Inhabilita en caso de no tener opciones
				disabledText: 'Seleccione para busqueda..', //Texto en caso de que no existan opciones
				maxHeight: 200, //Define el máximo de alto a desplegar
				buttonClass: 'selectBtn', //Selecciona la clase que se aplicará al botón
				numberDisplayed: 1, //Límite que se establece para ocupar el texto nSelectedText
				delimiterText: ';', //Separador de alternativas en el botón cuando numberDisplayed > 1
				selectedClass: 'selectedOption', //Clase de opciones seleccionadas
				includeSelectAllOption: true, //Agrega opción de seleccionar todas
				allSelectedText: 'Todos los valores', //Texto que se muestra cuando todas las opciones están seleccionadas
				nSelectedText: 'Valores seleccionados', //Texto cuando el límite de selección (numberDisplayed) es alcanzado
				selectAllText: 'Seleccionar todos los valores', //Texto de includeSelectAllOption
				enableFiltering: true, //Habilita el filtro en la cabecera
				enableCaseInsensitiveFiltering: true, //Inhabilita el case sensitive
				filterPlaceholder: 'Buscar...', //Placeholder del buscador
				onDropdownShow: function(event) {
					$(this.$select).parents('.form-group').addClass('focus')
				},
				onDropdownHide: function(event) {
					$(this.$select).parents('.form-group').removeClass('focus')
				},
			});
		});

$(function () { //Propiedades del DatePicker
	$('.datepicker').datetimepicker({
		stepping: 5, //Cantidad de saltos en el seleccionador de minutos
		//minDate: '12/20/2017 12:00', //Fecha Mínima de selección mm/dd/yyyy hh:mm
		//maxDate: '12/20/2017 12:00', //Fecha Máxima de selección mm/dd/yyyy hh:mm
		//defaultDate: '12/19/2017', //Fecha predeterminada
		//disabledDates: ['12/29/2017','01/01/2018'], //Fechas no seleccionables
		icons: { //Cambia los iconos
			close: 'glyphicon glyphicon-ok',
			clear: 'glyphicon glyphicon-remove'
			},
		format: 'DD/MM/YYYY', //Cambia el formato y puede eliminar el selector de tiempo
		useCurrent: true, //Usa la fecha y hora actual como predeterminado de selección
		sideBySide: false, //Si es true muestra el selector de fecha y tiempo juntos
		//daysOfWeekDisabled: [6,0], //Inhabilita Sábado y Domingo
		calendarWeeks: true, //Muestra el número de semana
		viewMode: 'days', //Selecciona la primera vista del datepicker. Valores: decades years months days
		showTodayButton: true, //Muestra el botón de HOY
		showClear: true, //Muestra el botón de limpiar el input
		showClose: true, //Muestra el botón de cerrar el datepicker
		ignoreReadonly: true, //Importante para no desplegar los teclados de los teléfonos
		widgetPositioning:{vertical: 'bottom'}, //Importante para que no moleste el menú superior
	})
})