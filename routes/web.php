<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('deposito', 'BancoController@deposito')->name('deposito');
Route::post('depositoUpdate', 'BancoController@depositoUpdate')->name('depositoUpdate');
Route::post('transferencia', 'BancoController@transferencia')->name('transferencia');
Route::post('saldo', 'BancoController@saldo')->name('saldo');
Route::post('historico', 'BancoController@historico')->name('historico');
Route::post('login', 'Usuarios@store')->name('login');
Route::get('/', function () {
    return view('welcome');
});
